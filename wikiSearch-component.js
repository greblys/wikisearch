﻿(function () {
    'use strict';

    const RESULT_URL = 'http://localhost:8080/w/api.php?format=json&action=query&generator=search&gsrnamespace=0&gsrlimit=10&prop=pageimages|extracts&pilimit=max&exintro&explaintext&exsentences=1&exlimit=max&gsrsearch=';
    
    angular.module('wikiApp', [])
        .component('wikiSearch', {
            templateUrl: 'wikiSearch.html',
            controller: wikiSearchController
        });

    wikiSearchController.$inject = ['$http'];

    function wikiSearchController($http) { 
        let ctrl = this;

        ctrl.$onInit = () => {
            ctrl.results = [];
        }

        ctrl.getSearchResults = () => {
            if (ctrl.searchTerm) {
                ctrl.noResults = false;
                ctrl.loading = true;
                ctrl.results = [];
                $http.get(RESULT_URL + ctrl.searchTerm)
                    .then(response => {
                        if (response.data && response.data.query) {
                            ctrl.results = Object.values(response.data.query.pages);
                        } else {
                            ctrl.noResults = true;
                        }
                    })
                    .catch(() => ctrl.noResults = true)
                    .finally(() => ctrl.loading = false);
            } 
        }

        ctrl.clearTerm = () => {
            ctrl.searchTerm = '';
            ctrl.results = [];
            ctrl.noResults = false;
        }
    }
})();
