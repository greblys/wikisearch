# Run instructions:

```
git clone https://gitlab.com/greblys/wikisearch.git
cd wikisearch
npm run build
npm run start
Open http://localhost:8080 on your browser
```


# Notes

## Fixed negatives:
* Bootstrap classes are used, but bootstrap not loaded.
* Same with WOW
* avoid $scope and use component structure so that it would be Angular friendly. It separates concerns better as well and easier to maintain.
* col-lg-10, col-md-10, col-xs-10 classes are redundant on the same element. Also, col-xs-10 is deprecated in Bootstrap 4. Same with other variations. col-10 is enough alone to achieve same result. 
col-lg-10 should be used when it is requirement to have specific layout only on large screens. col-10 is standard class which applies to all screen sizes, so it looks that the intention was to have same layout
on all screen sizes. In that case only col-10 is necessary and other classes are redundant.
* wouldn't wrap input elements with \<p\> elements. \<p\> elements should be used solely for text paragraphs.
* we are not processing entered values server side, so form element and name attribute loses a point
* rule of the thumb - avoid id attributes due to potential value conflicts (duplicate ids are HTML standards violation). Use class attribute instead and act on it appropriately with having duplicates in mind.
* label attribute is valid only with <option> element when defining selection dropdown menu.
* Input field with username references used as text input for search phrase. Removed username references.
* there is no button type clear
* dont use ngClass with static classes. ng-class="class" looked like some leftover when Javascript logic was removed for class property.  

## Positives:
* debounce is really good on text input fields to reduce overhead on Javascript/AngularJS computation resources. 200ms is also industry's standard.

## Future improvements:
* Since all rows are sharing same outer spacing then we could replace row and col-10 elements with single common container wrapper with common margin and width for all "rows". This would reduce number of
elements in the template and would make it to look cleaner. Kept col-10 and row elements because it looks that it was exercise's design to test understanding of these two classes.
* add unit tests
* Implement paging for wiki search results. Now only first page is displayed